<?php
header("Access-Control-Allow-Origin: *");
session_start();
$validFunctions = array(
    "Login",
    "Dashboard","GetAllExpenses","AddExpenses","GetAllincome","Addincome","GetAllincomename",
    "GetAllcustomers",
    "AddNewCustomer","AddNewdepositsCustomer","GetAllexpensesname","GetLoanvalueBycustomerdashboard","GetdepositvalueBycustomerdashboard",
    "GetCustomerName",
    "GetAllInterestLoans",
    "GenerateNewLoanNumber",
    "AddNewInterestLoan",
    "DeleteInterestLoan","Adddepositsprincipal","Addinterestfordeposits",
    "GetLoanDetailByLoannumber","sample","GetAlldepositscustomers",
    "GetAllLoanNo","GetindividualLoans","get_customerlist","get_depositscustomerlist","GetLoanvalueByLoannumber","AddexistingInterestLoan","AddexistingInterestLoanonlyint","GetindividualLoansbydailyloan","Getindividualdepositsloan","GetLoanvalueBycustomer","GetLoanvalueBydepositscustomer","Addexistingprinciplecustomer","Addinterest","gettodayreport","databasebackup"
   
);

$functName      = $_REQUEST['f'];
date_default_timezone_set('Asia/Kolkata');
$dt           = new DateTime();
$current_time = $dt->format('Y-m-d H:i:s');
$current_date = $dt->format('Y-m-d');

$local        = 'liveaa';

if ($local == 'live') {
    $dbhost  = '198.71.225.61:3306';
    $dbuser  = 'demofinance';
    $dbpass  = 'Prabhu@231';
    $db_name = 'demofinance';
    $conn    = mysqli_connect($dbhost, $dbuser, $dbpass, $db_name);
} else {
    $dbhost  = 'localhost';
    $dbuser  = 'root';
    $dbpass  = '';
    $db_name = 'jofinance';
    $conn    = mysqli_connect($dbhost, $dbuser, $dbpass, $db_name);
}

if (!$conn) {
    die('Could not connect: ' . mysql_error());
}

if (in_array($functName, $validFunctions)) {
    $functName();
} else {
    echo "You don't have permission to call that function so back off!";
    exit();
}

function Login()
{
    $username   = $_POST['UserName'];
    $password   = $_POST['Password'];
    
    $sql        = "SELECT * FROM `users` WHERE Email='{$username}' OR Mobile='{$username}' AND Password='{$password}'";
    $login      = mysqli_query($GLOBALS['conn'], $sql);
    $checklogin = mysqli_num_rows($login);
    $get_id     = mysqli_fetch_assoc($login);
    //$GetUserID  = $get_id['id'];
    //$GetUserID  = $get_id['Mobile'];
    if ($checklogin != 0) {
        $data = array();
        $data["status"]    = "success";
        $data["datavalue"] = $get_id;
        print_r(json_encode($data));
    } else {
        $data           = array();
        $data["status"] = "fail";
        print_r(json_encode($data));
    }    
}
function sendmessage($mobile, $otp)
{
    $msg    = 'Finance: Your OTP is ' . $otp;
    $Sender = 'iYuvaa';
    $msg    = str_replace(" ", "+", $msg);
    $toAra  = explode(",", $mobile);
    foreach ($toAra as $mob) {
        $ch = curl_init();
        $sms_url = 'http://smsyuvaa.in/api/sendhttp.php?authkey=190861AOdFoDB9eyfq5a4a4a66&mobiles=' . $mob . '&message=' . $msg . '&sender=' . $Sender . '&route=4&country=91';
        curl_setopt($ch, CURLOPT_URL, $sms_url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
    }
    $output = "";
}

function gettodayreport()
{
	$getrole = mysqli_query($GLOBALS['conn'], "select '' as cusname, CONCAT( 'Expenses For ' , ex.name) as name,ex.amount as debit,0 as credit,ex.remarks as remarks,ex.created_date as created_date  from expenses ex where ex.is_deleted=0 and ex.status='Active'  and DATE(ex.created_date) ='{$GLOBALS['current_date']}' ");
	 
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
	
	$getrole4 = mysqli_query($GLOBALS['conn'], "select '' as cusname,CONCAT( 'Other Income From ' , ex.name) as name,0 as debit,ex.amount  as credit,ex.remarks as remarks,ex.created_date as created_date  from income ex where ex.is_deleted=0 and ex.status='Active' and DATE(ex.created_date) ='{$GLOBALS['current_date']}' ");
	  
    while ($row4 = mysqli_fetch_assoc($getrole4)) {
        $data[] = $row4;
    }
	
	$getrole1 = mysqli_query($GLOBALS['conn'], "select cu.FullName as cusname, CASE
    WHEN ex.PayType = 'Principal' AND ex.Received = 0  THEN CONCAT( ' Loan Payment To ' ,  cu.FullName)  
     WHEN ex.PayType = 'Principal' AND ex.Paid = 0 THEN  CONCAT(' Loan Payment From ' ,  cu.FullName)
   else   ex.Received
END as name,ex.Paid as debit,ex.Received as credit,ex.remarks as remarks,ex.CreatedDate as created_date  from loandetails ex inner join customers cu on (cu.id=ex.CustomerID) where ex.PayType ='Principal' and  ex.IsDeleted=0  and DATE(ex.CreatedDate) ='{$GLOBALS['current_date']}'");
	
	 while ($row1 = mysqli_fetch_assoc($getrole1)) {
        $data[] = $row1;
    }
	
	$getrolein = mysqli_query($GLOBALS['conn'], "select cu.FullName as cusname,  CONCAT( 'Interest Received From  ' ,  cu.FullName) as name,ex.Paid as debit,ex.Received as credit,ex.remarks as remarks,ex.CreatedDate as created_date  from loandetails ex inner join customers cu on (cu.id=ex.CustomerID) where ex.PayType ='Interest' and ex.Received > 0 and  ex.IsDeleted=0  and DATE(ex.CreatedDate) ='{$GLOBALS['current_date']}'"); 
	 while ($row7 = mysqli_fetch_assoc($getrolein)) {
        $data[] = $row7;
    }
	
	$getrole14 = mysqli_query($GLOBALS['conn'], "select cu.FullName as cusname,CASE
    WHEN ex.PayType = 'Principal' AND ex.Received = 0  THEN CONCAT( 'Deposit Returned To ' ,  cu.FullName)  
     WHEN ex.PayType = 'Principal' AND ex.Paid = 0 THEN  CONCAT(' Deposit Received From ' ,  cu.FullName)
   else   ex.Received
END as name,ex.Received as credit,ex.Paid as debit,ex.remarks as remarks,ex.CreatedDate as created_date  from depositsdetails ex  inner join depositscustomers cu on (cu.id=ex.CustomerID) where ex.PayType ='Principal' and ex.IsDeleted=0  and DATE(ex.CreatedDate) ='{$GLOBALS['current_date']}'");
	
	 while ($row14 = mysqli_fetch_assoc($getrole14)) {
        $data[] = $row14;
    }
	
	$getrole17 = mysqli_query($GLOBALS['conn'], "select cu.FullName as cusname, CONCAT( 'Interest Paid To ' ,  cu.FullName)  as name,ex.Received as credit,ex.Paid as debit,ex.remarks as remarks,ex.CreatedDate as created_date  from depositsdetails ex  inner join depositscustomers cu on (cu.id=ex.CustomerID) where ex.PayType ='Interest' and  ex.Paid > 0  and ex.IsDeleted=0  and DATE(ex.CreatedDate) ='{$GLOBALS['current_date']}'");
	
	 while ($row17 = mysqli_fetch_assoc($getrole17)) {
        $data[] = $row17;
    }
	
    $opt["data"] = $data;
    print_r(json_encode($opt));
}
function Dashboard()
{
    $gettotal = mysqli_query($GLOBALS['conn'], "select (select count(*) from customers) as customer,(select count(loannumber) from interestloans where Status='Active' and IsDeleted='0') as loans,(select sum(loanamount) from interestloans where Status='Active' and IsDeleted='0') as loanoutstanding");
    $get_totalrecord = mysqli_fetch_assoc($gettotal);

    $getrecordaa = mysqli_query($GLOBALS['conn'], "select paid-rec as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and IsDeleted='0') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and IsDeleted='0') as rec) as sss"); 
    $get_idsa     = mysqli_fetch_assoc($getrecordaa); 


    $getinterest = mysqli_query($GLOBALS['conn'], "select paid-rec as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `loandetails` WHERE PayType='Interest' and IsDeleted='0' ) as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `loandetails` WHERE PayType='Interest' and IsDeleted='0') as rec) as sss"); 
    $getinterestsa     = mysqli_fetch_assoc($getinterest); 



    $getrecordww = mysqli_query($GLOBALS['conn'], "select rec-paid as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and IsDeleted='0' ) as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal'   and IsDeleted='0') as rec) as sss"); 
    $get_idww    = mysqli_fetch_assoc($getrecordww); 

    $getrecordsasa = mysqli_query($GLOBALS['conn'], "select rec-paid as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Interest' and IsDeleted='0' ) as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Interest' and IsDeleted='0') as rec) as sss"); 
    $getrecordsasasa    = mysqli_fetch_assoc($getrecordsasa);  
	
	$getcurrentmonthexpen = mysqli_query($GLOBALS['conn'], "select COALESCE(sum(amount),'0') as amount  from expenses where MONTH(created_date) = MONTH(CURRENT_DATE())"); 
    $getcurrentmonthexpenval    = mysqli_fetch_assoc($getcurrentmonthexpen);  
	

    $apiUrl = 'http://smsyuvaa.in/api/balance.php?authkey=190861AOdFoDB9eyfq5a4a4a66&type=4';
    $curl = curl_init($apiUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $data["customer"]        = $get_totalrecord['customer'];
    $data["loansoutstandingprincipal"]           = $get_idsa['bal']; 
    $data["loanoutstandinginterest"]           = GetLoanvalueBycustomerdashboard();//$getinterestsa['bal']; 
    $data["depositoutstandingprincipal"]           = $get_idww['bal'];
    $data["currentmonth"]           = date("F") ; //$date('M Y');
    $data["depositoutstandinginteresr"]           = GetdepositvalueBycustomerdashboard();//$getrecordsasasa['bal']; 
    $data["currentmontexpens"] = $getcurrentmonthexpenval['amount'];
    $data["smsbalance"]      = curl_exec($curl);
    print_r(json_encode($data));
}
function GetAllexpensesname()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select Distinct name from  expenses");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
  ///  $opt["data"] = $data;
    print_r(json_encode($data));
}

function GetAllincomename()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select Distinct name from  income");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
  ///  $opt["data"] = $data;
    print_r(json_encode($data));
}
function GetAllcustomers()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select C.InterestRate,C.id,C.FullName,C.Address,C.City,C.Pincode,C.Mobile,C.Email,CG.GroupName,C.Remarks,C.Status from customers as C inner join customergroups as CG on C.CustomerGroupID = CG.ID where C.IsDeleted='0'");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    $opt["data"] = $data;
    print_r(json_encode($opt));
}
function GetAllExpenses()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select c.name,c.amount,c.remarks,c.created_date,c.created_by,c.status,c.is_deleted,CG.FullName from expenses as c inner join users as CG on c.created_by = CG.id where c.is_deleted='0'");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    $opt["data"] = $data;
    print_r(json_encode($opt));
}


function GetAllincome()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select c.name,c.amount,c.remarks,c.created_date,c.created_by,c.status,c.is_deleted,CG.FullName from income as c inner join users as CG on c.created_by = CG.id where c.is_deleted='0'");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    $opt["data"] = $data;
    print_r(json_encode($opt));
}


function GetAlldepositscustomers()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select C.InterestRate,C.id,C.FullName,C.Address,C.City,C.Pincode,C.Mobile,C.Email,CG.GroupName,C.Remarks,C.Status from depositscustomers as C inner join customergroups as CG on C.CustomerGroupID = CG.ID where C.IsDeleted='0'");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    $opt["data"] = $data;
    print_r(json_encode($opt));
}


function get_customerlist()
{
    $getrole = mysqli_query($GLOBALS['conn'], "SELECT id,FullName  FROM `customers` ");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    print_r(json_encode($data));
    
}

function get_depositscustomerlist()
{
    $getrole = mysqli_query($GLOBALS['conn'], "SELECT id,FullName  FROM `depositscustomers` ");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    print_r(json_encode($data));
}

function AddExpenses()
{
    $expname     = $_POST['expname'];
    $expamount      = $_POST['expamount'];
    $expremarks         = $_POST['expremarks'];
    $created_by      = $_POST['created_by']; 
    $insert = mysqli_query($GLOBALS['conn'], "insert into expenses(name,amount,remarks,created_by,created_date,status,is_deleted) values ('{$expname}','{$expamount}','{$expremarks}','{$created_by}','{$GLOBALS['current_time']}','Active','0')");
    if ($insert) {
        $opt["status"] = "success";
        print_r(json_encode($opt));
    } else {
        $opt["status"] = "fail";
        print_r(json_encode($opt));
    }
}

function Addincome()
{
    $expname     = $_POST['expname'];
    $expamount      = $_POST['expamount'];
    $expremarks         = $_POST['expremarks'];
    $created_by      = $_POST['created_by']; 
    $insert = mysqli_query($GLOBALS['conn'], "insert into income(name,amount,remarks,created_by,created_date,status,is_deleted) values ('{$expname}','{$expamount}','{$expremarks}','{$created_by}','{$GLOBALS['current_time']}','Active','0')");
    if ($insert) {
        $opt["status"] = "success";
        print_r(json_encode($opt));
    } else {
        $opt["status"] = "fail";
        print_r(json_encode($opt));
    }
}

function AddNewCustomer()
{
    $FullName     = $_POST['FullName'];
    $Address      = $_POST['Address'];
    $City         = $_POST['City'];
    $Pincode      = $_POST['Pincode'];
    $Mobile       = $_POST['Mobile'];
    $Email        = $_POST['Email'];
    $Remarks      = $_POST['Remarks'];
    $txtinterestrate      = $_POST['txtinterestrate'];
    
    $sql        = "SELECT FullName FROM `customers` WHERE FullName='{$FullName}'";
    $login      = mysqli_query($GLOBALS['conn'], $sql);
    $checklogin = mysqli_num_rows($login);
    if($checklogin == 0)
    { 
        $insert = mysqli_query($GLOBALS['conn'], "insert into customers(InterestRate,FullName,Address,City,Pincode,Mobile,Email,Remarks,Status,IsDeleted) values ('{$txtinterestrate}','{$FullName}','{$Address}','{$City}','{$Pincode}','{$Mobile}','{$Email}','{$Remarks}','Active','0')");
        if ($insert) {
            $opt["status"] = "success";
            print_r(json_encode($opt));
        } else {
            $opt["status"] = "fail";
            print_r(json_encode($opt));
        }
    }
    else
    {
        $opt["status"] = "alreadyexist";
        print_r(json_encode($opt));
    }
}

function AddNewdepositsCustomer()
{
    $FullName     = $_POST['FullName'];
    $Address      = $_POST['Address'];
    $City         = $_POST['City'];
    $Pincode      = $_POST['Pincode'];
    $Mobile       = $_POST['Mobile'];
    $Email        = $_POST['Email'];
    $Remarks      = $_POST['Remarks'];
    $txtinterestrate      = $_POST['txtinterestrate'];
    
    $sql        = "SELECT FullName FROM `depositscustomers` WHERE FullName='{$FullName}'";
    $login      = mysqli_query($GLOBALS['conn'], $sql);
    $checklogin = mysqli_num_rows($login);
    if($checklogin == 0)
    { 
        $insert = mysqli_query($GLOBALS['conn'], "insert into depositscustomers(InterestRate,FullName,Address,City,Pincode,Mobile,Email,Remarks,Status,IsDeleted) values ('{$txtinterestrate}','{$FullName}','{$Address}','{$City}','{$Pincode}','{$Mobile}','{$Email}','{$Remarks}','Active','0')");
        if ($insert) {
            $opt["status"] = "success";
            print_r(json_encode($opt));
        } else {
            $opt["status"] = "fail";
            print_r(json_encode($opt));
        }
    }
    else
    {
        $opt["status"] = "alreadyexist";
        print_r(json_encode($opt));
    }
}
function GetCustomerName()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select id,FullName  from customers ");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    print_r(json_encode($data));
}

function GetLoanDetailByLoannumber()
{
    $LN     = $_POST['loannumber'];
    $getrole = mysqli_query($GLOBALS['conn'], "select id,loannumber from interestloans where CustomerID='{$LN}'");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    
    print_r(json_encode($data));
}

function GetAllLoanNo()
{   
    $getrole = mysqli_query($GLOBALS['conn'], "select id,loannumber from interestloans");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    print_r(json_encode($data));
}


function GenerateNewLoanNumber()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select property_value from res_setting where property_name='loanno'");
    
    $get_billid     = mysqli_fetch_assoc($getrole);
    $data["billno"] = $get_billid['property_value'];
    print_r(json_encode($data));
}


function AddNewInterestLoan()
{
    $CustomerID     = $_POST['CustomerID'];
    $LoanNumber     = $_POST['LoanNumber'];
    $LoanDate       = $_POST['LoanDate'];
    $LoanAmount     = $_POST['LoanAmount'];
   // $Frequency      = $_POST['Frequency'];
    $InterestRate   = $_POST['InterestRate1'];
    $InterestPerMonth = $_POST['InterestAmount'];
    $InterestPerDay   = $_POST['InterestDate'];
    $Remarks        = $_POST['Remarks'];
   // $created_by     = $_POST['CreatedBy1']; 
    $sas = explode("/",$LoanDate);
    $LoanDate = $sas[2]."/".$sas[1]."/".$sas[0]; 

    $insert = mysqli_query($GLOBALS['conn'], "INSERT INTO `interestloans`(`CustomerID`, `loannumber`, `LoanDate`, `loanamount`, `Frequency`, `interestrate`, `InterestPerMonth`, `InterestPerDay`, `remarks`, `status`, `IsDeleted`,LoanOrgDate,loanorgamount) VALUES ('{$CustomerID}','{$LoanNumber}','{$LoanDate}','{$LoanAmount}','3','{$InterestRate}','{$InterestPerMonth}','{$InterestPerDay}','{$Remarks}','Active','0','{$LoanDate}','{$LoanAmount}')");
    $Update = mysqli_query($GLOBALS['conn'], "INSERT INTO `iloantransactions`(`CustomerID`, `InterestLoanID`, `Date`, `Received`, `Paid`, `Remarks`, `Status`, `IsDeleted`) VALUES ('{$CustomerID}','{$LoanNumber}','{$LoanDate}','0.00','{$LoanAmount}','{$Remarks}','Success','0')");

    if ($insert) {
        $updateloanno = mysqli_query($GLOBALS['conn'], "update  res_setting set property_value=property_value+'1',updated_date='{$GLOBALS['current_time']}' where property_name='loanno'");
        $opt["status"] = "success";
        print_r(json_encode($opt));
    } else {
        $opt["status"] = "fail";
        print_r(json_encode($opt));
    }
}

function AddexistingInterestLoan()
{
    $uptodateinterest = $_POST['uptodateinterest'];
    $customerid = $_POST['customerid'];
    $loanno = $_POST['loanno'];
    $amount = $_POST['amount'];
    $txtRemarks = $_POST['txtRemarks'];
    $Created_by = $_POST['Created_by'];
 
    if($uptodateinterest > 0)
    {        
        $Update = mysqli_query($GLOBALS['conn'], "INSERT INTO `iloantransactions`(`CustomerID`, `InterestLoanID`, `Date`, `Received`, `Paid`, `Remarks`, `Status`, `IsDeleted`,CreatedBy) VALUES ('{$customerid}','{$loanno}','{$GLOBALS['current_date']}',  '0.00','{$uptodateinterest}','Interest','Success','0','{$Created_by }')"); 
        $maintable = mysqli_query($GLOBALS['conn'], "update interestloans set totalinterest=totalinterest+'{$uptodateinterest}' where loannumber='{$loanno}'");
    } 
   
    $Update1 = mysqli_query($GLOBALS['conn'], "INSERT INTO `iloantransactions`(`CustomerID`, `InterestLoanID`, `Date`, `Paid`, `Received`, `Remarks`, `Status`, `IsDeleted`,CreatedBy) VALUES ('{$customerid}','{$loanno}','{$GLOBALS['current_date']}','0.00','{$amount}','{$txtRemarks}','Success','0','{$Created_by }')");
   
    $maintable = mysqli_query($GLOBALS['conn'], "update interestloans set LoanDate='{$GLOBALS['current_date']}',loanamount= loanamount-'{$amount}' where loannumber='{$loanno}'");  


    if ($maintable) {
     //   $updateloanno = mysqli_query($GLOBALS['conn'], "update  res_setting set property_value=property_value+'1',updated_date='{$GLOBALS['current_time']}' where property_name='loanno'");
     $getrole = mysqli_query($GLOBALS['conn'], "SELECT loanamount,interestrate,InterestPerDay FROM `interestloans` where loannumber='{$loanno}'");
     $data     = mysqli_fetch_assoc($getrole);
     $val = $data['loanamount'] * $data['interestrate'];
     $val1 =     $val / 100;
     $val2 = $val1 / 30;
     $interperdat = mysqli_query($GLOBALS['conn'], "update  interestloans set InterestPerMonth='{$val1}', InterestPerDay='{$val2}' where loannumber='{$loanno}'");

     $opt["status"] = "success";
        print_r(json_encode($opt));
    } else {
        $opt["status"] = "fail";
        print_r(json_encode($opt));
    }

}
function  AddexistingInterestLoanonlyint()
{
    $uptodateinterest = $_POST['uptodateinterest'];
    $customerid = $_POST['customerid'];
    $loanno = $_POST['loanno'];
    $amount = $_POST['amount'];
    $txtRemarks = $_POST['txtRemarks'];
    $Created_by = $_POST['Created_by'];
 
  if($uptodateinterest > 0)
    {         
        $Update = mysqli_query($GLOBALS['conn'], "INSERT INTO `iloantransactions`(`CustomerID`, `InterestLoanID`, `Date`, `Received`, `Paid`, `Remarks`, `Status`, `IsDeleted`,CreatedBy) VALUES ('{$customerid}','{$loanno}','{$GLOBALS['current_date']}',  '0.00','{$uptodateinterest}','Interest','Success','0','{$Created_by }')"); 
    }
        $Update = mysqli_query($GLOBALS['conn'], "INSERT INTO `iloantransactions`(`CustomerID`, `InterestLoanID`, `Date`, `Paid`, `Received`, `Remarks`, `Status`, `IsDeleted`,CreatedBy) VALUES ('{$customerid}','{$loanno}','{$GLOBALS['current_date']}',  '0.00','{$amount}','{$txtRemarks}','Success','0','{$Created_by }')"); 
        $maintable = mysqli_query($GLOBALS['conn'], "update interestloans set totalinterest=totalinterest+'{$uptodateinterest}' where loannumber='{$loanno}'");
        $maintable = mysqli_query($GLOBALS['conn'], "update interestloans set LoanDate='{$GLOBALS['current_date']}' where loannumber='{$loanno}'");  
        if($maintable)
        {
        $opt["status"] = "success";
        print_r(json_encode($opt));
        } else {
            $opt["status"] = "fail";
            print_r(json_encode($opt));
        } 
   /*  } */
}
function GetAllInterestLoans()
{
    $getrole = mysqli_query($GLOBALS['conn'], "select IL.ID,C.FullName,IL.loannumber,IL.LoanDate,IL.loanamount,(CT.termname)as Frequency,IL.interestrate,IL.InterestPerMonth,IL.InterestPerDay,IL.remarks,IL.status from interestloans as IL inner join customers as C on IL.CustomerID = C.ID inner join collectionterms as CT on IL.Frequency = CT.id where IL.IsDeleted='0'");
    $data    = array();
    while ($row = mysqli_fetch_assoc($getrole)) {
        $data[] = $row;
    }
    $opt["data"] = $data;
    print_r(json_encode($opt));
}


function DeleteInterestLoan()
{   
    $InterestLoanID        = $_POST['InterestLoanID'];    
    $Update = mysqli_query($GLOBALS['conn'], "UPDATE `interestloans` SET Isdeleted='1' WHERE ID = '{$InterestLoanID}'");
    if ($Update) {
        $opt["status"] = "success";
        print_r(json_encode($opt));
    } else {
        $opt["status"] = "fail";
        print_r(json_encode($opt));
    }
}


function GetindividualLoans()
{
    $loanno = $_POST['loanno'];
    
    if ($loanno == '') {
      //  $getinentryrecord = mysqli_query($GLOBALS['conn'], "select lrp.loannumber,lrp.emino,lrp.emidate,lrp.emiamount,lrp.remarks,lrp.status,lrp.paid_date,c.FullName from loanrepayment as lrp inner join customers as c on lrp.customer = c.id where lrp.is_deleted='False'");
      $data = array();
    } else {
        $getinentryrecord = mysqli_query($GLOBALS['conn'], "SELECT DATE_format(Date, '%d/%m/%Y') as Date,CustomerID,InterestLoanID,ID,Received,Paid,Remarks,Status,CreatedDate FROM iloantransactions where InterestLoanID='{$loanno}'");
         $data = array();
      while ($row = mysqli_fetch_assoc($getinentryrecord)) {
            $data[] = $row;
        } 
    } 
    $opt["data"] = $data;
  
    print_r(json_encode($opt));
}

function GetLoanvalueByLoannumber()
{
    $loanno = $_POST['loannumber'];
  //  $loanno = "287";
    date_default_timezone_set('Asia/Kolkata');
    $dt           = new DateTime();
    $current_time = $dt->format('Y-m-d');
    $getrole = mysqli_query($GLOBALS['conn'], "SELECT loanamount,loannumber,LoanDate,interestrate,InterestPerDay,totalinterest FROM `interestloans` where loannumber='{$loanno}'");
    $data     = mysqli_fetch_assoc($getrole);
    $opt["billno"] =  $data ; 
  //  $opt["interest"] =  $data ['loanamount'];
    $loandate =  $data ['LoanDate'];      
    $loandatecon = strtotime($loandate);
    $currentdate = strtotime($current_time);
    $d = $currentdate - $loandatecon;
    $totalday = $d / 86400; 
    $interpertday = $data['InterestPerDay']; 
    $totalinterest = $interpertday * $totalday;
    $opt["interest"] =  $totalinterest ; 
    $opt["totalday"] =  $totalday ; 
        print_r(json_encode($opt));
}



function Getindividualdepositsloan()
{
    $txtcustomer = $_POST['txtcustomer'];
    
    if ($txtcustomer == '') {
      //  $getinentryrecord = mysqli_query($GLOBALS['conn'], "select lrp.loannumber,lrp.emino,lrp.emidate,lrp.emiamount,lrp.remarks,lrp.status,lrp.paid_date,c.FullName from loanrepayment as lrp inner join customers as c on lrp.customer = c.id where lrp.is_deleted='False'");
      $data = array();
    } else {
        $getinentryrecord = mysqli_query($GLOBALS['conn'], "SELECT ID,DATE_format(Date, '%d/%m/%Y') as Date,CustomerID,Description,PayType,Received,Paid,Others,Balance,Remarks,Status,CreatedDate FROM depositsdetails where CustomerID='{$txtcustomer}'");
         $data = array();
      while ($row = mysqli_fetch_assoc($getinentryrecord)) {
            $data[] = $row;
        } 
    } 
    $opt["data"] = $data;
  
    print_r(json_encode($opt));
}

function GetindividualLoansbydailyloan()
{
    $txtcustomer = $_POST['txtcustomer'];
    
    if ($txtcustomer == '') {
      //  $getinentryrecord = mysqli_query($GLOBALS['conn'], "select lrp.loannumber,lrp.emino,lrp.emidate,lrp.emiamount,lrp.remarks,lrp.status,lrp.paid_date,c.FullName from loanrepayment as lrp inner join customers as c on lrp.customer = c.id where lrp.is_deleted='False'");
      $data = array();
    } else {
        $getinentryrecord = mysqli_query($GLOBALS['conn'], "SELECT ID,DATE_format(Date, '%d/%m/%Y') as Date,CustomerID,Description,PayType,Received,Paid,Others,Balance,Remarks,Status,CreatedDate FROM loandetails where CustomerID='{$txtcustomer}' and ISDeleted=0");
         $data = array();
      while ($row = mysqli_fetch_assoc($getinentryrecord)) {
            $data[] = $row;
        } 
    } 
    $opt["data"] = $data;
  
    print_r(json_encode($opt));
}



function GetLoanvalueBydepositscustomer()
{
     $customerid = $_POST['customerid'];
    //$customerid = "2";
    date_default_timezone_set('Asia/Kolkata');
    $dt           = new DateTime();
    $current_time = $dt->format('Y-m-d');
    $getrole = mysqli_query($GLOBALS['conn'], "select recesum-ppaidsum as outstandingprinciple,rinterestsum-pinterestsum as outstandinginterest,lastdate,interestrate  from (select  (SELECT  COALESCE(sum(Paid),'0') as ppaidsum FROM `depositsdetails` where PayType='Principal' and  CustomerID='{$customerid}') as ppaidsum, (SELECT  COALESCE(sum(Received),'0') as ppaidsum FROM `depositsdetails` where PayType='Principal' and  CustomerID='{$customerid}') as recesum  ,(SELECT  COALESCE(sum(Paid),'0') as pinterestsum FROM `depositsdetails` where PayType='Interest' and  CustomerID='{$customerid}') as pinterestsum,(SELECT  COALESCE(sum(Received),'0') as pinterestsum FROM `depositsdetails` where PayType='Interest' and  CustomerID='{$customerid}') as rinterestsum,(SELECT  COALESCE(DATE_format(Date, '%d/%m/%Y'),' ') as lastdate FROM `depositsdetails` where   CustomerID='{$customerid}' order by id desc limit 1) as lastdate,(SELECT  COALESCE(InterestRate,'0') as interestrate FROM `depositscustomers` where   id='{$customerid}' ) as interestrate) as sss ");

    $data     = mysqli_fetch_assoc($getrole);
    $opt["billno"] =  $data; 
  //  $opt["interest"] =  $data ['loanamount'];
  //if()
  if($data ['lastdate'] != '')
  { 
      $sasa = explode("/",$data ['lastdate']); 
      $exactdate = $sasa[2]."-".$sasa[1]."-".$sasa[0]; 
    $loandate =  $exactdate;      
    $loandatecon = strtotime($loandate);
    $currentdate = strtotime($current_time);
    $d = $currentdate - $loandatecon;
    $totalday = $d / 86400; 
//echo $totalday ;
    $interpertday = $data['interestrate']; 
    $totalinterest = $interpertday * $totalday;

    
    $val = $data['outstandingprinciple'] * $data['interestrate'];
                    $val1 =     $val / 100;
                    $val2 = $val1 / 30;  
                    $aaa =  $totalday * $val2 + $data['outstandinginterest']; 

    $opt["interest"] =  $totalinterest ; 
    $opt["uptodateinterest"] =  $aaa ; 
    $opt["noofdays"] =  $totalday ;  
  }
  else
  {
    $opt["uptodateinterest"] =  0;
    $opt["interest"] =  0 ; 
    $opt["noofdays"] = 0 ;  
  }
     
        print_r(json_encode($opt));
}

function GetLoanvalueBycustomer()
{
     $customerid = $_POST['customerid'];
    //$customerid = "2";
    date_default_timezone_set('Asia/Kolkata');
    $dt           = new DateTime();
    $current_time = $dt->format('Y-m-d');
    $getrole = mysqli_query($GLOBALS['conn'], "select ppaidsum-recesum as outstandingprinciple,pinterestsum-rinterestsum as outstandinginterest,lastdate,interestrate  from (select  (SELECT  COALESCE(sum(Paid),'0') as ppaidsum FROM `loandetails` where isdeleted=0 and  PayType='Principal' and  CustomerID='{$customerid}') as ppaidsum, (SELECT  COALESCE(sum(Received),'0') as ppaidsum FROM `loandetails`   where isdeleted=0 and PayType='Principal' and  CustomerID='{$customerid}') as recesum  ,(SELECT  COALESCE(sum(Paid),'0') as pinterestsum FROM `loandetails` where isdeleted=0 and  PayType='Interest' and  CustomerID='{$customerid}') as pinterestsum,(SELECT  COALESCE(sum(Received),'0') as pinterestsum FROM `loandetails` where isdeleted=0 and  PayType='Interest' and  CustomerID='{$customerid}') as rinterestsum,(SELECT  COALESCE(DATE_format(Date, '%d/%m/%Y'),' ') as lastdate FROM `loandetails` where   isdeleted=0 and CustomerID='{$customerid}' order by id desc limit 1) as lastdate,(SELECT  COALESCE(InterestRate,'0') as interestrate FROM `customers` where   id='{$customerid}' ) as interestrate) as sss ");

    $data     = mysqli_fetch_assoc($getrole);
    $opt["billno"] =  $data; 
  //  $opt["interest"] =  $data ['loanamount'];
  //if()
  if($data ['lastdate'] != '')
  { 
      $sasa = explode("/",$data ['lastdate']); 
      $exactdate = $sasa[2]."-".$sasa[1]."-".$sasa[0]; 
    $loandate =  $exactdate;      
    $loandatecon = strtotime($loandate);
    $currentdate = strtotime($current_time);
    $d = $currentdate - $loandatecon;
    $totalday = $d / 86400; 

    $interpertday = $data['interestrate']; 
    $totalinterest = $interpertday * $totalday;

    
    $val = $data['outstandingprinciple'] * $data['interestrate'];
                    $val1 =     $val / 100;
                    $val2 = $val1 / 30;  
                    $aaa =  $totalday * $val2 + $data['outstandinginterest']; 

    $opt["interest"] =  $totalinterest ; 
    $opt["uptodateinterest"] =  $aaa ; 
    $opt["noofdays"] =  $totalday ;  
  }
  else
  {
    $opt["uptodateinterest"] =  0;
    $opt["interest"] =  0 ; 
    $opt["noofdays"] = 0 ;  
  }
     
        print_r(json_encode($opt));
}


function sample()

{
    $str1 = 'jan10';
$str2 = 'jan10';
    //if(strcasecmp('string1', 'string1') == 0)
    if($str1 == $str2)
    {
         echo 'Strings match.';
    } else {
         echo 'Strings do not match. ';
    }
    /* 
    $str1 = 'jan10';
$str2 = 'jan10';
// compare the first three letters of the two strings above
echo strncmp($str1, $str2); */
}
function Addexistingprinciplecustomer()
{
    $uptodateinterest = $_POST['uptodateinterest'];
    $paymenttype = trim($_POST['paymenttype']);
    $customerid = $_POST['customerid']; 
    $principlLoanDate = $_POST['principalLoanDate']; 
    $lastdate = $_POST['lastdate'];     
    $amount = $_POST['amount'];
    $txtRemarks = $_POST['txtRemarks'];
    $Created_by = $_POST['Created_by'];
    $interestrate= $_POST['interestrate'];
    $outstandingprinciple= $_POST['outstandingprinciple'];

    $ppp = "Principal ".$paymenttype; 
    
    $sql        = "SELECT ID FROM `loandetails` WHERE  CustomerID='{$customerid}'";
    $login      = mysqli_query($GLOBALS['conn'], $sql);
    $checklogin = mysqli_num_rows($login);   
    $sa = "Received";  
    $totalday = 0;

            if ($checklogin == 0 && $paymenttype == $sa) 
            {
                $opt["status"] = "notallow";
                print_r(json_encode($opt));
            }
            else
            {   
                 $selecteddate = explode( "/",$principlLoanDate );
                $selecteddate1 = $selecteddate[2]."-".$selecteddate[1]."-".$selecteddate[0];
              
                if($lastdate != '')
                {
                    $lastupdateddate = explode( "/",$lastdate);
                    $lastupdateddate = $lastupdateddate[2]."-".$lastupdateddate[1]."-".$lastupdateddate[0]; 
                    $loanupdateddatecon = strtotime($lastupdateddate);
                    $sedate = strtotime($selecteddate1);
                    $d = $sedate - $loanupdateddatecon;
                    $totalday = $d / 86400; 

                     $val = $outstandingprinciple * $interestrate;
                    $val1 =     $val / 100;
                    $val2 = $val1 / 30;  
                    $aaa =  $totalday * $val2; 
                    if($totalday > 0)
                    {     
                        $getrecorda = mysqli_query($GLOBALS['conn'], "select paid-rec as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
                        $get_ids     = mysqli_fetch_assoc($getrecorda); 
                        $sam="Interest Added For ".$totalday." days - ".$interestrate."";
                        $insteres = mysqli_query($GLOBALS['conn'], "INSERT INTO `loandetails`(Balance,Description,`CustomerID`, `Date`,PayType,Paid, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$get_ids['bal']}','{$sam}','{$customerid}','{$selecteddate1}', 'Interest', '{$aaa}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 
 
                    } 
                }
                 
           $getrecord = mysqli_query($GLOBALS['conn'], "select paid-rec as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
           $get_id     = mysqli_fetch_assoc($getrecord); 
            
                        if ($paymenttype == $sa)   
                    {
                        $value_exact = $get_id['bal'] - $amount;    
                    } 
                    else
                    {
                        $value_exact = $get_id['bal'] + $amount;
                    }
            $Update = mysqli_query($GLOBALS['conn'], "INSERT INTO `loandetails`(Balance,Description,`CustomerID`, `Date`,PayType,{$paymenttype}, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$value_exact}','{$ppp}','{$customerid}','{$selecteddate1}', 'Principal', '{$amount}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 
                if($Update)
                {
                    $opt["status"] = "success";
                    print_r(json_encode($opt));
                } else {
                    $opt["status"] = "fail";
                    print_r(json_encode($opt));
                }
        }
    }

    function Addinterestfordeposits()
    {
        $uptodateinterest = $_POST['uptodateinterest'];
        $paymenttype = trim($_POST['paymenttype']);
        $customerid = $_POST['customerid']; 
        $interestLoanDate = $_POST['interestLoanDate']; 
        $lastdate = $_POST['lastdate'];     
        $amount = $_POST['amount'];
        $txtRemarks = $_POST['txtRemarks'];
        $Created_by = $_POST['Created_by'];
        $interestrate= $_POST['interestrate'];
        $outstandingprinciple= $_POST['outstandingprinciple'];
    
    
        $selecteddate = explode( "/",$interestLoanDate );
        $selecteddate1 = $selecteddate[2]."-".$selecteddate[1]."-".$selecteddate[0];
    
    
        if($lastdate != '')
        {
            $lastupdateddate = explode( "/",$lastdate);
            $lastupdateddate = $lastupdateddate[2]."-".$lastupdateddate[1]."-".$lastupdateddate[0]; 
            $loanupdateddatecon = strtotime($lastupdateddate);
            $sedate = strtotime($selecteddate1);
            $d = $sedate - $loanupdateddatecon;
            $totalday = $d / 86400; 
    
             $val = $outstandingprinciple * $interestrate;
            $val1 =     $val / 100;
            $val2 = $val1 / 30;  
            $aaa =  $totalday * $val2; 
            if($totalday > 0)
            {     
                $getrecordaa = mysqli_query($GLOBALS['conn'], "select rec-paid as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
                $get_idsa     = mysqli_fetch_assoc($getrecordaa); 
                $sama="Interest Added For ".$totalday." days - ".$interestrate."";
                $insteres = mysqli_query($GLOBALS['conn'], "INSERT INTO `depositsdetails`(Balance,Description,`CustomerID`, `Date`,PayType,Received, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$get_idsa['bal']}','{$sama}','{$customerid}','{$selecteddate1}', 'Interest', '{$aaa}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 
    
            } 
        }
    
        $getrecorda = mysqli_query($GLOBALS['conn'], "select rec-paid as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
                            $get_ids     = mysqli_fetch_assoc($getrecorda); 
                            $sam="Interest Received";
                            $insteres = mysqli_query($GLOBALS['conn'], "INSERT INTO `depositsdetails`(Balance,Description,`CustomerID`, `Date`,PayType,Paid, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$get_ids['bal']}','{$sam}','{$customerid}','{$selecteddate1}', 'Interest', '{$amount}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 
    
                            if($insteres)
                            {
                                $opt["status"] = "success";
                                print_r(json_encode($opt));
                            } else {
                                $opt["status"] = "fail";
                                print_r(json_encode($opt));
                            }
                        }
function Adddepositsprincipal()
{
    $uptodateinterest = $_POST['uptodateinterest'];
    $paymenttype = trim($_POST['paymenttype']);
    $customerid = $_POST['customerid']; 
    $principlLoanDate = $_POST['principalLoanDate']; 
    $lastdate = $_POST['lastdate'];     
    $amount = $_POST['amount'];
    $txtRemarks = $_POST['txtRemarks'];
    $Created_by = $_POST['Created_by'];
    $interestrate= $_POST['interestrate'];
    $outstandingprinciple= $_POST['outstandingprinciple'];

    $ppp = "Principal ".$paymenttype; 
    
    $sql        = "SELECT ID FROM `depositsdetails` WHERE  CustomerID='{$customerid}'";
    $login      = mysqli_query($GLOBALS['conn'], $sql);
    $checklogin = mysqli_num_rows($login);   
    $sa = "Paid";  
    $totalday = 0;

            if ($checklogin == 0 && $paymenttype == $sa) 
            {
                $opt["status"] = "notallow";
                print_r(json_encode($opt));
            }
            else
            {   
                 $selecteddate = explode( "/",$principlLoanDate );
                $selecteddate1 = $selecteddate[2]."-".$selecteddate[1]."-".$selecteddate[0];
              
                if($lastdate != '')
                {
                    $lastupdateddate = explode( "/",$lastdate);
                    $lastupdateddate = $lastupdateddate[2]."-".$lastupdateddate[1]."-".$lastupdateddate[0]; 
                    $loanupdateddatecon = strtotime($lastupdateddate);
                    $sedate = strtotime($selecteddate1);
                    $d = $sedate - $loanupdateddatecon;
                    $totalday = $d / 86400; 

                     $val = $outstandingprinciple * $interestrate;
                    $val1 =     $val / 100;
                    $val2 = $val1 / 30;  
                    $aaa =  $totalday * $val2; 
                    if($totalday > 0)
                    {     
                        $getrecorda = mysqli_query($GLOBALS['conn'], "select rec-paid as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
                        $get_ids     = mysqli_fetch_assoc($getrecorda); 
                        $sam="Interest Added For ".$totalday." days - ".$interestrate."";
                        $insteres = mysqli_query($GLOBALS['conn'], "INSERT INTO `depositsdetails`(Balance,Description,`CustomerID`, `Date`,PayType,Received, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$get_ids['bal']}','{$sam}','{$customerid}','{$selecteddate1}', 'Interest', '{$aaa}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 
 
                    } 
                }
                 
           $getrecord = mysqli_query($GLOBALS['conn'], "select rec-paid as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `depositsdetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
           $get_id     = mysqli_fetch_assoc($getrecord); 
            
                        if ($paymenttype == $sa)   
                    {
                        $value_exact = $get_id['bal'] - $amount;    
                    } 
                    else
                    {
                        $value_exact = $get_id['bal'] + $amount;
                    }
            $Update = mysqli_query($GLOBALS['conn'], "INSERT INTO `depositsdetails`(Balance,Description,`CustomerID`, `Date`,PayType,{$paymenttype}, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$value_exact}','{$ppp}','{$customerid}','{$selecteddate1}', 'Principal', '{$amount}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 
                if($Update)
                {
                    $opt["status"] = "success";
                    print_r(json_encode($opt));
                } else {
                    $opt["status"] = "fail";
                    print_r(json_encode($opt));
                }
        }
    }

        function Addinterest()
{
    $uptodateinterest = $_POST['uptodateinterest'];
    $paymenttype = trim($_POST['paymenttype']);
    $customerid = $_POST['customerid']; 
    $interestLoanDate = $_POST['interestLoanDate']; 
    $lastdate = $_POST['lastdate'];     
    $amount = $_POST['amount'];
    $txtRemarks = $_POST['txtRemarks'];
    $Created_by = $_POST['Created_by'];
    $interestrate= $_POST['interestrate'];
    $outstandingprinciple= $_POST['outstandingprinciple'];


    $selecteddate = explode( "/",$interestLoanDate );
    $selecteddate1 = $selecteddate[2]."-".$selecteddate[1]."-".$selecteddate[0];


    if($lastdate != '')
    {
        $lastupdateddate = explode( "/",$lastdate);
        $lastupdateddate = $lastupdateddate[2]."-".$lastupdateddate[1]."-".$lastupdateddate[0]; 
        $loanupdateddatecon = strtotime($lastupdateddate);
        $sedate = strtotime($selecteddate1);
        $d = $sedate - $loanupdateddatecon;
        $totalday = $d / 86400; 

         $val = $outstandingprinciple * $interestrate;
        $val1 =     $val / 100;
        $val2 = $val1 / 30;  
        $aaa =  $totalday * $val2; 
        if($totalday > 0)
        {     
            $getrecordaa = mysqli_query($GLOBALS['conn'], "select paid-rec as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
            $get_idsa     = mysqli_fetch_assoc($getrecordaa); 
            $sama="Interest Added For ".$totalday." days - ".$interestrate."";
            $insteres = mysqli_query($GLOBALS['conn'], "INSERT INTO `loandetails`(Balance,Description,`CustomerID`, `Date`,PayType,Paid, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$get_idsa['bal']}','{$sama}','{$customerid}','{$selecteddate1}', 'Interest', '{$aaa}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 

        } 
    }

    $getrecorda = mysqli_query($GLOBALS['conn'], "select paid-rec as bal from (select (SELECT COALESCE(sum(Paid),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as paid, (SELECT COALESCE(sum(Received),'0') as paidvalue FROM `loandetails` WHERE PayType='Principal' and CustomerID='{$customerid}') as rec) as sss"); 
                        $get_ids     = mysqli_fetch_assoc($getrecorda); 
                        $sam="Interest Reeceived";
                        $insteres = mysqli_query($GLOBALS['conn'], "INSERT INTO `loandetails`(Balance,Description,`CustomerID`, `Date`,PayType,Received, `Remarks`,  `IsDeleted`,CreatedBy,CreatedDate) VALUES ('{$get_ids['bal']}','{$sam}','{$customerid}','{$selecteddate1}', 'Interest', '{$amount}','{$txtRemarks}','0','{$Created_by }','{$GLOBALS['current_time']}')"); 

                        if($insteres)
                        {
                            $opt["status"] = "success";
                            print_r(json_encode($opt));
                        } else {
                            $opt["status"] = "fail";
                            print_r(json_encode($opt));
                        } 

}



function GetLoanvalueBycustomerdashboard()
{ 
    date_default_timezone_set('Asia/Kolkata');
    $dt           = new DateTime();
    $current_time = $dt->format('Y-m-d');
	$totalvalue=0;  
    $data = array();
    $getrole = mysqli_query($GLOBALS['conn'],"Select *,(SELECT  COALESCE(InterestRate,'0') as interestrate FROM `customers` where   id=ss.CustomerID) as ssss,(SELECT  COALESCE(DATE_format(Date, '%d/%m/%Y'),' ') as lastdate FROM `loandetails` where   CustomerID=ss.CustomerID and IsDeleted='0' order by id desc limit 1) as lastdate,(SELECT  COALESCE(sum(Paid),'0') as pinterestsum FROM `loandetails` where PayType='Interest' and IsDeleted='0' and  CustomerID=ss.CustomerID) as pinterestsum,(SELECT  COALESCE(sum(Received),'0') as pinterestsum FROM `loandetails` where PayType='Interest' and IsDeleted='0' and   CustomerID=ss.CustomerID) as rinterestsum from (select CustomerID,sum(Paid-Received) as ss from loandetails where PayType='Principal' and IsDeleted='0'group by CustomerID) as ss"); 
        while ($row = mysqli_fetch_assoc($getrole)) {
             $data[] = $row;
               if($row ['lastdate'] != '')
             {    
				$rowinterest = $row['pinterestsum'] - $row['rinterestsum'];
				$sasa = explode("/",$row ['lastdate']); 
                 $exactdate = $sasa[2]."-".$sasa[1]."-".$sasa[0]; 
               $loandate =  $exactdate;      
               $loandatecon = strtotime($loandate);
               $currentdate = strtotime($current_time);
               $d = $currentdate - $loandatecon;
               $totalday = $d / 86400;  
               $interpertday = $row['ssss']; 
               $totalinterest = $interpertday * $totalday;  
               $val = $row['ss'] * $row['ssss'];
                               $val1 =     $val / 100;
                               $val2 = $val1 / 30;  
                               $aaa =  $totalday * $val2 +$rowinterest;//+ $row['outstandinginterest'];  
               $opt["interest"] =  $totalinterest ; 
               $opt["uptodateinterest"] =  $aaa ; 
               $opt["noofdays"] =  $totalday ;  
			  $totalvalue= $totalvalue + $aaa;  
             }          
        }  
		return round($totalvalue,2); 
    exit; 
}

function GetdepositvalueBycustomerdashboard()
{ 
    date_default_timezone_set('Asia/Kolkata');
    $dt           = new DateTime();
    $current_time = $dt->format('Y-m-d');
	$totalvalue=0;  
    $data = array();
	
	  //$getrole = mysqli_query($GLOBALS['conn'], "select recesum-ppaidsum as outstandingprinciple,rinterestsum-pinterestsum as outstandinginterest,lastdate,interestrate  from (select  (SELECT  COALESCE(sum(Paid),'0') as ppaidsum FROM `depositsdetails` where PayType='Principal' and  CustomerID='{$customerid}') as ppaidsum, (SELECT  COALESCE(sum(Received),'0') as ppaidsum FROM `depositsdetails` where PayType='Principal' and  CustomerID='{$customerid}') as recesum  ,(SELECT  COALESCE(sum(Paid),'0') as pinterestsum FROM `depositsdetails` where PayType='Interest' and  CustomerID='{$customerid}') as pinterestsum,(SELECT  COALESCE(sum(Received),'0') as pinterestsum FROM `depositsdetails` where PayType='Interest' and  CustomerID='{$customerid}') as rinterestsum,(SELECT  COALESCE(DATE_format(Date, '%d/%m/%Y'),' ') as lastdate FROM `depositsdetails` where   CustomerID='{$customerid}' order by id desc limit 1) as lastdate,(SELECT  COALESCE(InterestRate,'0') as interestrate FROM `depositscustomers` where   id='{$customerid}' ) as interestrate) as sss ");
    $getrole = mysqli_query($GLOBALS['conn'],"Select *,(SELECT  COALESCE(InterestRate,'0') as interestrate FROM `customers` where   id=ss.CustomerID) as ssss,(SELECT  COALESCE(DATE_format(Date, '%d/%m/%Y'),' ') as lastdate FROM `depositsdetails` where   CustomerID=ss.CustomerID order by id desc limit 1) as lastdate,(SELECT  COALESCE(sum(Paid),'0') as pinterestsum FROM `depositsdetails` where PayType='Interest' and  CustomerID=ss.CustomerID) as pinterestsum,(SELECT  COALESCE(sum(Received),'0') as pinterestsum FROM `depositsdetails` where PayType='Interest' and  CustomerID=ss.CustomerID) as rinterestsum from (select CustomerID,sum(Received-Paid) as ss from depositsdetails where PayType='Principal' group by CustomerID) as ss"); 
        while ($row = mysqli_fetch_assoc($getrole)) {
             $data[] = $row;
               if($row ['lastdate'] != '')
             {    //rinterestsum-pinterestsum 
				$rowinterest = $row['rinterestsum'] - $row['pinterestsum'];
				$sasa = explode("/",$row ['lastdate']); 
                 $exactdate = $sasa[2]."-".$sasa[1]."-".$sasa[0]; 
               $loandate =  $exactdate;      
               $loandatecon = strtotime($loandate);
               $currentdate = strtotime($current_time);
               $d = $currentdate - $loandatecon;
               $totalday = $d / 86400;  
               $interpertday = $row['ssss']; 
               $totalinterest = $interpertday * $totalday;  
               $val = $row['ss'] * $row['ssss'];
                               $val1 =     $val / 100;
                               $val2 = $val1 / 30;  
                               $aaa =  $totalday * $val2 +$rowinterest;//+ $row['outstandinginterest'];  
               $opt["interest"] =  $totalinterest ; 
               $opt["uptodateinterest"] =  $aaa ; 
               $opt["noofdays"] =  $totalday ;  
			  $totalvalue= $totalvalue + $aaa;  
             }          
        }  
		return round($totalvalue,2); 
    exit; 
}


function databasebackup()
{  
$host = $GLOBALS['dbhost'];
$username = $GLOBALS['dbuser'];
$password = $GLOBALS['dbpass'];
$database_name = $GLOBALS['db_name'];
  

// Get connection object and set the charset
$conn = mysqli_connect($host, $username, $password, $database_name);
$conn->set_charset("utf8");


// Get All Table Names From the Database
$tables = array();
$sql = "SHOW TABLES";
$result = mysqli_query($conn, $sql);

while ($row = mysqli_fetch_row($result)) {
    $tables[] = $row[0];
}

$sqlScript = "";
foreach ($tables as $table) {
    
    // Prepare SQLscript for creating table structure
    $query = "SHOW CREATE TABLE $table";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_row($result);
    
    $sqlScript .= "\n\n" . $row[1] . ";\n\n";
    
    
    $query = "SELECT * FROM $table";
    $result = mysqli_query($conn, $query);
    
    $columnCount = mysqli_num_fields($result);
    
    // Prepare SQLscript for dumping data for each table
    for ($i = 0; $i < $columnCount; $i ++) {
        while ($row = mysqli_fetch_row($result)) {
            $sqlScript .= "INSERT INTO $table VALUES(";
            for ($j = 0; $j < $columnCount; $j ++) {
                $row[$j] = $row[$j];
                
                if (isset($row[$j])) {
                    $sqlScript .= '"' . $row[$j] . '"';
                } else {
                    $sqlScript .= '""';
                }
                if ($j < ($columnCount - 1)) {
                    $sqlScript .= ',';
                }
            }
            $sqlScript .= ");\n";
        }
    }
    
    $sqlScript .= "\n"; 
}

if(!empty($sqlScript))
{
	//echo "success"; 
    // Save the SQL script to a backup file
    $backup_file_name = $database_name . '_backup_' . time() . '.sql';
    $fileHandler = fopen($backup_file_name, 'w+');
    $number_of_lines = fwrite($fileHandler, $sqlScript);
    fclose($fileHandler); 

    // Download the SQL backup file to the browser
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($backup_file_name));
    ob_clean();
    flush();
    readfile($backup_file_name);
    exec('rm ' . $backup_file_name);  
} 

}

?>