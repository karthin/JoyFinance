-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 25, 2018 at 08:22 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finance`
--

-- --------------------------------------------------------

--
-- Table structure for table `collectionterms`
--

CREATE TABLE `collectionterms` (
  `id` int(11) NOT NULL,
  `termname` varchar(50) NOT NULL,
  `days` int(11) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collectionterms`
--

INSERT INTO `collectionterms` (`id`, `termname`, `days`, `description`, `status`) VALUES
(1, 'Daily', 1, '', 'Active'),
(2, 'Weekly', 7, '', 'Active'),
(3, 'Monthly', 30, '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `customergroups`
--

CREATE TABLE `customergroups` (
  `ID` int(11) NOT NULL,
  `GroupName` varchar(100) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customergroups`
--

INSERT INTO `customergroups` (`ID`, `GroupName`, `Description`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(0, 'General', '', 'Active', '2018-03-18 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `OrganizationID` int(11) NOT NULL,
  `CustomerGroupID` int(11) NOT NULL,
  `InterestRate` double NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `Address` varchar(1000) NOT NULL,
  `City` varchar(500) NOT NULL,
  `Pincode` varchar(10) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Mobile` varchar(25) NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(100) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `OrganizationID`, `CustomerGroupID`, `InterestRate`, `FullName`, `Address`, `City`, `Pincode`, `Email`, `Mobile`, `Remarks`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(1, 0, 0, 0, 'PRABHU', '', '', '', '', '', '', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 0, 0, 'Viky', '', '', '', '', '', '', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 0, 0, 'Karthi', '', '', '', '', '', '', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `iloantransactions`
--

CREATE TABLE `iloantransactions` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `InterestLoanID` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Received` double NOT NULL,
  `Paid` double NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(200) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iloantransactions`
--

INSERT INTO `iloantransactions` (`ID`, `CustomerID`, `InterestLoanID`, `Date`, `Received`, `Paid`, `Remarks`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(1, 1, 305, '2018-03-01 00:00:00', 0, 150000, '', 'Success', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(2, 1, 305, '2018-03-24 00:00:00', 0, 3450, 'Interest', 'Success', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(3, 1, 305, '2018-03-24 00:00:00', 3450, 0, '', 'Success', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(4, 1, 305, '2018-03-24 00:00:00', 0, 1200, 'Interest', 'Success', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(5, 1, 305, '2018-03-24 00:00:00', 50000, 0, '', 'Success', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(6, 1, 305, '2018-03-24 00:00:00', 1200, 0, '', 'Success', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `interestloans`
--

CREATE TABLE `interestloans` (
  `id` int(11) NOT NULL,
  `OrganizationID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `loannumber` varchar(11) NOT NULL,
  `LoanDate` date NOT NULL,
  `loanamount` double NOT NULL,
  `Frequency` int(11) NOT NULL,
  `interestrate` double NOT NULL,
  `InterestPerMonth` double NOT NULL,
  `InterestPerDay` double NOT NULL,
  `remarks` varchar(1000) NOT NULL,
  `status` varchar(50) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL,
  `LoanOrgDate` date NOT NULL,
  `loanorgamount` double NOT NULL,
  `totalinterest` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interestloans`
--

INSERT INTO `interestloans` (`id`, `OrganizationID`, `CustomerID`, `loannumber`, `LoanDate`, `loanamount`, `Frequency`, `interestrate`, `InterestPerMonth`, `InterestPerDay`, `remarks`, `status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`, `LoanOrgDate`, `loanorgamount`, `totalinterest`) VALUES
(1, 0, 1, '305', '2018-03-24', 100000, 3, 3, 3000, 100, '', 'Active', '2018-03-24 11:57:24', 0, '0000-00-00 00:00:00', 0, 0, '2018-03-01', 150000, 4650);

-- --------------------------------------------------------

--
-- Table structure for table `loandetails`
--

CREATE TABLE `loandetails` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Description` varchar(500) NOT NULL,
  `PayType` varchar(100) NOT NULL,
  `Paid` double NOT NULL,
  `Received` double NOT NULL,
  `Others` double NOT NULL,
  `Balance` double NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(100) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loandetails`
--

INSERT INTO `loandetails` (`ID`, `CustomerID`, `Date`, `Description`, `PayType`, `Paid`, `Received`, `Others`, `Balance`, `Remarks`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(1, 2, '2018-03-15', 'Amount Paid For 1.50 Interest Rate', 'Principal', 150000, 0, 0, 150000, '', '', '2018-03-25 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(2, 2, '2018-03-25', 'Interest Added For 10 Days', 'Interest', 1500, 0, 0, 151500, '', '', '2018-03-25 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(3, 2, '2018-03-25', 'Principal Received', 'Principal', 0, 50000, 0, 101500, '', '', '2018-03-25 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(4, 2, '2018-03-25', 'Interest Received', 'Interest', 0, 1500, 0, 100000, '', '', '2018-03-25 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `res_setting`
--

CREATE TABLE `res_setting` (
  `id` int(11) NOT NULL,
  `OrganizationID` int(11) NOT NULL,
  `prefix` varchar(50) NOT NULL,
  `property_name` varchar(50) NOT NULL,
  `property_value` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `is_deleted` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `res_setting`
--

INSERT INTO `res_setting` (`id`, `OrganizationID`, `prefix`, `property_name`, `property_value`, `description`, `created_date`, `updated_date`, `created_by`, `updated_by`, `status`, `is_deleted`) VALUES
(1, 0, '', 'loanno', '306', '', '2017-08-25 22:05:00', '2018-03-24 17:25:01', '', '', 'active', 'f'),
(2, 0, '', 'outentry_receiptno', '1', 'For out entry Receipt no', '2017-12-21 11:00:00', '2017-12-26 10:28:20', '1', '1', 'active', 'f');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `FullName` varchar(250) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Mobile` varchar(50) NOT NULL,
  `Password` varchar(250) NOT NULL,
  `Role` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FullName`, `Email`, `Mobile`, `Password`, `Role`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(1, 'Prabhu', '1', '9940715788', '1', 1, 1, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collectionterms`
--
ALTER TABLE `collectionterms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customergroups`
--
ALTER TABLE `customergroups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iloantransactions`
--
ALTER TABLE `iloantransactions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `interestloans`
--
ALTER TABLE `interestloans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loandetails`
--
ALTER TABLE `loandetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collectionterms`
--
ALTER TABLE `collectionterms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customergroups`
--
ALTER TABLE `customergroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `iloantransactions`
--
ALTER TABLE `iloantransactions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `interestloans`
--
ALTER TABLE `interestloans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `loandetails`
--
ALTER TABLE `loandetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `AutoUser` ON SCHEDULE EVERY 5 SECOND STARTS '2018-03-24 09:05:00' ON COMPLETION NOT PRESERVE DISABLE DO INSERT INTO `users` (`FullName`) VALUES ('24121')$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
