
function login() {
  var name = $('#username').val()
  var password = $('#password').val()
  if (name == '') {
    swal("Oops...", "Name Should not be Null", "error");
  }
  else if (password == '') {
    swal("Oops...", "Password Should not be Null", "error");
  }
  else {
    var dataString = "&UserName=" + name + "&Password=" + password;
    $.ajax({
      type: "POST",
      url: base_url + "Login",
      data: dataString,
      crossDomain: true,
      cache: false,
      beforeSend: function () {
      },
      success: function (ret) {
        var data = JSON.parse(ret);
        if ($.trim(data.status) == "success") {
          localStorage.setItem("userid", data.datavalue.id);
          localStorage.setItem("username", data.datavalue.FullName);
          localStorage.setItem("datavalue", data.datavalue);
          window.location.replace("dashboard.html");
          // swal({
          //   title: "Lucky Finance!",
          //   text: "Please enter your OTP,You have recieved:",
          //   type: "input",
          //   showCancelButton: false,
          //   closeOnConfirm: false,
          //   animation: "slide-from-top",
          //   inputPlaceholder: "Enter OTP"
          // },
          //   function (inputValue) {


          //     if (inputValue === "") {
          //       swal.showInputError("OTP should not empty!");
          //       return false
          //     }

          //     else if (inputValue != "") {

          //       if (inputValue === $.trim(data.otp)) {
          //         swal("Nice!", "Entered OTP is correct", "success");
          //         localStorage.setItem("userid", data.datavalue.id);
          //         localStorage.setItem("username", data.datavalue.FullName);
          //         localStorage.setItem("datavalue", data.datavalue);
          //         window.location.replace("dashboard.html");
          //       }
          //       else {
          //         swal.showInputError("Entered OTP is Wrong!!");
          //         return false;

          //       }

            //     if (inputValue === false)
            //       return false;
            //   }

            // });


        }
        else {
          swal("Oops!", "Invalid Username and Password!", "error");
        }
      }
    });
  }
}
