-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2018 at 12:59 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finance`
--

-- --------------------------------------------------------

--
-- Table structure for table `collectionterms`
--

CREATE TABLE `collectionterms` (
  `id` int(11) NOT NULL,
  `termname` varchar(50) NOT NULL,
  `days` int(11) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collectionterms`
--

INSERT INTO `collectionterms` (`id`, `termname`, `days`, `description`, `status`) VALUES
(1, 'Daily', 1, '', 'Active'),
(2, 'Weekly', 7, '', 'Active'),
(3, 'Monthly', 30, '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `customergroups`
--

CREATE TABLE `customergroups` (
  `ID` int(11) NOT NULL,
  `GroupName` varchar(100) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customergroups`
--

INSERT INTO `customergroups` (`ID`, `GroupName`, `Description`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(0, 'General', '', 'Active', '2018-03-18 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `OrganizationID` int(11) NOT NULL,
  `CustomerGroupID` int(11) NOT NULL,
  `InterestRate` double NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `Address` varchar(1000) NOT NULL,
  `City` varchar(500) NOT NULL,
  `Pincode` varchar(10) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Mobile` varchar(25) NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(100) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `OrganizationID`, `CustomerGroupID`, `InterestRate`, `FullName`, `Address`, `City`, `Pincode`, `Email`, `Mobile`, `Remarks`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(1, 0, 0, 2, 'Prabhu', '', '', '', '', '', '', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 0, 2.1, 'bharathi', 'lakshmipuram', 'cbe', '641044', '', '9698783000', '\n\n', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 0, 2.1, 'mahesh', '', 'cbe', '', '', '', '', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(4, 0, 0, 2.1, 'srichakra', '', 'cbe', '641044', '', '', '', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(5, 0, 0, 1, 'karthi', 'sdasd', 'Coimbatore', '641027', 'karthin1391@gmail.com', '7894561231', 'karthi', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 0, 2, 'fdfd', 'sdfsd', 'fdsadf', '343543', 'karthin1381', '78945612332', 'cvdsgv', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `depositscustomers`
--

CREATE TABLE `depositscustomers` (
  `id` int(11) NOT NULL,
  `OrganizationID` int(11) NOT NULL,
  `CustomerGroupID` int(11) NOT NULL,
  `InterestRate` double NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `Address` varchar(1000) NOT NULL,
  `City` varchar(500) NOT NULL,
  `Pincode` varchar(10) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Mobile` varchar(25) NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(100) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `depositscustomers`
--

INSERT INTO `depositscustomers` (`id`, `OrganizationID`, `CustomerGroupID`, `InterestRate`, `FullName`, `Address`, `City`, `Pincode`, `Email`, `Mobile`, `Remarks`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(5, 0, 0, 9, 'dsfa', 'dsfgds', '435435', '435435', 's@gmai.com', '2132434', 'sdfvd', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 0, 1, 'cxzvxc', 'dsgfdg', 'coim', '641027', 'zxcx@gmail.com', '3434324', 'dvdg', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(7, 0, 0, 2, 'sdsa', 'fdgfd', 'sgfdg', '45435', 'dsfvdg', '234432432', '', 'Active', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `depositsdetails`
--

CREATE TABLE `depositsdetails` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `PayType` varchar(50) NOT NULL,
  `Received` double(18,2) NOT NULL,
  `Paid` double(18,2) NOT NULL,
  `Balance` double(18,2) NOT NULL,
  `Others` double(18,2) NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `depositsdetails`
--

INSERT INTO `depositsdetails` (`ID`, `CustomerID`, `Date`, `Description`, `PayType`, `Received`, `Paid`, `Balance`, `Others`, `Remarks`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(2, 5, '2018-03-01', 'Principal Received', 'Principal', 150000.00, 0.00, 150000.00, 0.00, '', '', '2018-03-31 15:39:35', 1, '0000-00-00 00:00:00', 0, 0),
(3, 5, '2018-03-31', 'Interest Added For 30 days - 9.00', 'Interest', 13500.00, 0.00, 150000.00, 0.00, '', '', '2018-03-31 15:40:41', 1, '0000-00-00 00:00:00', 0, 0),
(4, 5, '2018-03-31', 'Principal Paid', 'Principal', 0.00, 50000.00, 100000.00, 0.00, '', '', '2018-03-31 15:40:41', 1, '0000-00-00 00:00:00', 0, 0),
(5, 5, '2018-03-31', 'Interest Received', 'Interest', 0.00, 13500.00, 100000.00, 0.00, 'sadasd', '', '2018-03-31 16:24:54', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `amount` double NOT NULL,
  `remarks` varchar(150) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `is_deleted` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `name`, `amount`, `remarks`, `created_date`, `updated_date`, `created_by`, `updated_by`, `status`, `is_deleted`) VALUES
(1, 'Test', 2000, '', '2018-03-31 13:15:03', '0000-00-00 00:00:00', '1', '', 'Active', '0');

-- --------------------------------------------------------

--
-- Table structure for table `iloantransactions`
--

CREATE TABLE `iloantransactions` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `InterestLoanID` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Received` double NOT NULL,
  `Paid` double NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(200) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `amount` double NOT NULL,
  `remarks` varchar(150) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `is_deleted` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`id`, `name`, `amount`, `remarks`, `created_date`, `updated_date`, `created_by`, `updated_by`, `status`, `is_deleted`) VALUES
(1, 'Test', 125555, '', '2018-03-14 13:23:00', '0000-00-00 00:00:00', '1', '', 'Active', '0');

-- --------------------------------------------------------

--
-- Table structure for table `interestloans`
--

CREATE TABLE `interestloans` (
  `id` int(11) NOT NULL,
  `OrganizationID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `loannumber` varchar(11) NOT NULL,
  `LoanDate` date NOT NULL,
  `loanamount` double NOT NULL,
  `Frequency` int(11) NOT NULL,
  `interestrate` double NOT NULL,
  `InterestPerMonth` double NOT NULL,
  `InterestPerDay` double NOT NULL,
  `remarks` varchar(1000) NOT NULL,
  `status` varchar(50) NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL,
  `LoanOrgDate` date NOT NULL,
  `loanorgamount` double NOT NULL,
  `totalinterest` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loandetails`
--

CREATE TABLE `loandetails` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Description` varchar(1000) NOT NULL,
  `PayType` varchar(50) NOT NULL,
  `Received` double(18,2) NOT NULL,
  `Paid` double(18,2) NOT NULL,
  `Balance` double(18,2) NOT NULL,
  `Others` double(18,2) NOT NULL,
  `Remarks` varchar(1000) NOT NULL,
  `Status` varchar(50) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loandetails`
--

INSERT INTO `loandetails` (`ID`, `CustomerID`, `Date`, `Description`, `PayType`, `Received`, `Paid`, `Balance`, `Others`, `Remarks`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(6, 5, '2018-03-01', 'Principal Paid', 'Principal', 0.00, 100000.00, 100000.00, 0.00, '', '', '2018-03-31 15:15:01', 1, '0000-00-00 00:00:00', 0, 0),
(7, 5, '2018-03-14', 'Interest Added For 13 days - 1.00', 'Interest', 0.00, 433.33, 100000.00, 0.00, '', '', '2018-03-31 15:15:10', 1, '0000-00-00 00:00:00', 0, 0),
(8, 5, '2018-03-14', 'Principal Received', 'Principal', 50000.00, 0.00, 50000.00, 0.00, '', '', '2018-03-31 15:15:10', 1, '0000-00-00 00:00:00', 0, 0),
(9, 5, '2018-03-31', 'Interest Added For 17 days - 1.00', 'Interest', 0.00, 283.33, 50000.00, 0.00, '', '', '2018-03-31 15:15:46', 1, '0000-00-00 00:00:00', 0, 0),
(10, 5, '2018-03-31', 'Interest Reeceived', 'Interest', 716.66, 0.00, 50000.00, 0.00, '', '', '2018-03-31 15:15:46', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `res_setting`
--

CREATE TABLE `res_setting` (
  `id` int(11) NOT NULL,
  `OrganizationID` int(11) NOT NULL,
  `prefix` varchar(50) NOT NULL,
  `property_name` varchar(50) NOT NULL,
  `property_value` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_by` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `is_deleted` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `res_setting`
--

INSERT INTO `res_setting` (`id`, `OrganizationID`, `prefix`, `property_name`, `property_value`, `description`, `created_date`, `updated_date`, `created_by`, `updated_by`, `status`, `is_deleted`) VALUES
(1, 0, '', 'loanno', '5', '', '2017-08-25 22:05:00', '2018-03-24 18:25:43', '', '', 'active', 'f'),
(2, 0, '', 'outentry_receiptno', '1', 'For out entry Receipt no', '2017-12-21 11:00:00', '2017-12-26 10:28:20', '1', '1', 'active', 'f');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `FullName` varchar(250) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Mobile` varchar(50) NOT NULL,
  `Password` varchar(250) NOT NULL,
  `Role` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL,
  `IsDeleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FullName`, `Email`, `Mobile`, `Password`, `Role`, `Status`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`, `IsDeleted`) VALUES
(1, 'Prabhu', '1', '9940715788', '1', 1, 1, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collectionterms`
--
ALTER TABLE `collectionterms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customergroups`
--
ALTER TABLE `customergroups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depositscustomers`
--
ALTER TABLE `depositscustomers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depositsdetails`
--
ALTER TABLE `depositsdetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iloantransactions`
--
ALTER TABLE `iloantransactions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interestloans`
--
ALTER TABLE `interestloans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loandetails`
--
ALTER TABLE `loandetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collectionterms`
--
ALTER TABLE `collectionterms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customergroups`
--
ALTER TABLE `customergroups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `depositscustomers`
--
ALTER TABLE `depositscustomers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `depositsdetails`
--
ALTER TABLE `depositsdetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `iloantransactions`
--
ALTER TABLE `iloantransactions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `interestloans`
--
ALTER TABLE `interestloans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loandetails`
--
ALTER TABLE `loandetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
