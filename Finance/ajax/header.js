
var user_id = localStorage.getItem("userid");
var username = localStorage.getItem("username");

var base_userid = user_id;
if (user_id == '' || user_id == null) {
    window.location.replace("index.html");
}

$("#header").append('<nav class="navbar-default navbar-static-side" role="navigation"> ' +
    ' <div class="sidebar-collapse">' +
    '<ul class="nav metismenu" id="side-menu">' +
    ' <li class="nav-header">' +
    ' <div class="dropdown profile-element"> <span>' +
    ' <img alt="image" class="img-circle" src="img/profile_small.jpg" />' +
    ' </span>' +
    '    <a data-toggle="dropdown" class="dropdown-toggle" href="#">' +
    '<span class="clear">' +
    ' <span class="block m-t-xs"><strong class="font-bold">' + username + '</strong>' +
    '  </span> <span class="text-muted text-xs block">Tech Admin<b class="caret"></b></span> </span> </a>' +
    '    <ul class="dropdown-menu animated fadeInRight m-t-xs"> ' +
    '  <li class="divider"></li>' +
    ' <li><a logout>Logout</a></li>' +
    '  </ul>' +
    ' </div>' +
    '<div class="logo-element">' +
    ' '+shortname+' ' +
    ' </div>' +
    ' </li>' +
    '<li class="active">' +
    '<a href="Dashboard.html"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>' +
    '</li>' +
    //     '<li>'+
    //        ' <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Masters</span><span class="fa arrow"></span></a>'+
    //         '<ul class="nav nav-second-level collapse">'+
    //          '   <li><a href="bin.html">Bin Add/Edit</a></li>'+
    //          '   <li><a href="PartNo.html">Part No Add/Edit</a></li>'+
    //         '    <li><a href="supplier.html">Supplier Add/Edit</a></li>'+
    //         '    <li><a href="employee.html">Employee Add/Edit</a></li>'+
    //         '    <li><a href="roles.html">Role Add/Edit</a></li>'+
    //         '    <li><a href="bininward.html">Bin Inward Add/Edit</a></li>'+
    //                                  '</ul>'+
    //    ' </li>'+
    '  <li>' +
    '     <a href="groups.html"><i class="fa fa-users"></i> <span class="nav-label">Customer Groups</span></a>' +
    '                        </li>' +
    '  <li>' +
    '     <a href="customers.html"><i class="fa fa-user-plus"></i> <span class="nav-label">Customers</span></a>' +
    '                        </li>' +
    '  <li>' +
    '  <li>' +
    '     <a href="expenses.html"><i class="fa fa-google-wallet"></i> <span class="nav-label">Add Expenses</span></a>' +
    '                        </li>' +
    '  <li>' +
    '     <a href="loans.html"><i class="fa fa-inr"></i> <span class="nav-label">New Installment Loan</span></a>' +
    '                        </li>' +
    '  <li>' +
    '     <a href="interestloans.html"><i class="fa fa-inr"></i> <span class="nav-label">New Interest Loan</span></a>' +
    '                        </li>' +
    '  <li>' +
    '     <a href="loancollections.html"><i class="fa fa-history"></i> <span class="nav-label">EMI Collection</span></a>' +
    '                        </li>' +
    ' <li> ' +
    '   <a href="collectionreport.html"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Collection Report</span></a> ' +
    '                       </li>' +
    ' <li> ' +
    '   <a href="pendingreport.html"><i class="fa fa-envelope"></i> <span class="nav-label">Overall Pendings Report</span></a> ' +
    '                       </li>' +
    //         '<li>'+
    //         '    <a href="outentry.html"><i class="fa fa-pie-chart"></i> <span class="nav-label">Out Entry</span>  </a>'+
    //   '      </li>'+
    //       '  <li>'+
    //          '   <a href="Outentryprint.html"><i class="fa fa-flask"></i> <span class="nav-label">Out Entry Print</span></a>'+
    //         '</li>'+
    //     '<li>'+
    //      '   <a href="#"><i class="fa fa-edit"></i> <span class="nav-label">Reports</span><span class="fa arrow"></span></a>'+
    //    '     <ul class="nav nav-second-level collapse">'+
    //    '   <li><a href="inentryprint.html">In Entry Report</a></li>'+
    //    '   <li><a href="Outentryprint.html">Out Entry Report</a></li>'+
    //        '     <li><a href="#">Stock Report</a></li>'+
    //        '     <li><a href="#">Login Report</a></li>'+
    //          '   <li><a href="#">Bins Report</a></li>'+ 
    //                           '       </ul>'+
    //  '   </li>'+                   

    // '<li class="landing_link">' +
    // '<a target="_blank" href="#"><i class="fa fa-star"></i> <span class="nav-label">Orders</span> <span class="label label-warning pull-right">NEW</span></a>' +
    // '</li>' +
    '       </ul>' +

    '       </div>' +
    '  </nav> ');


$("body").on("click", "[logout]", function () {
    sessionStorage.clear();
    localStorage.clear();

    localStorage.setItem("userid", "");
    localStorage.setItem("username", "");
    localStorage.setItem("datavalue", "");
    window.location.replace("index.html");
});


$("#nav").append('<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">' +
    ' <div class="navbar-header">' +
    '    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>' +
    '    <form role="search" class="navbar-form-custom" action="search_results.html">' +
    '    <div class="form-group">' +
    '   <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">' +
    '    </div>' +
    '    </form>' +
    '</div>' +
    '<ul class="nav navbar-top-links navbar-right">' +
    '<li>' +
    '<span class="m-r-sm text-muted welcome-message">Welcome to ' + project_name + '</span>' +
    '   </li>' +
    '    <li>' +
    '       <a logout>' +
    '          <i class="fa fa-sign-out"></i> Log out' +
    '     </a>' +
    '     </li>' +
    '    <li>' +
    '       <a class="right-sidebar-toggle">' +
    '          <i class="fa fa-tasks"></i>' +
    '     </a>' +
    '    </li>' +
    '</ul>' +
    '</nav>');


$("#footer").append(' <div class="footer">' +
    '      <div class="pull-right">' +
    '            '+companyname+'' +
    '          </div>' +
    '          <div>' +
    '            <strong>Copyright</strong>'+productname+' &copy; '+year+'' +
    '          </div>' +
    '      </div>');
  